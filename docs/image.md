# Images

- [Corbis Images](http://www.corbisimages.com/)  
- [Lorel Pixel](http://lorempixel.com/)  
- [PlaceholdIt](http://placehold.it/)

## Free Images

- [Splitshire](http://splitshire.com/)
- [Picjumbo](http://picjumbo.com/)
- [Unsplash](http://unsplash.com/)
- [New Old Stock](http://nos.twnsnd.co/)
- [Refe](http://getrefe.tumblr.com/)
- [Public Domain Archive](http://publicdomainarchive.com/)
- [Jay Mantri](http://jaymantri.com/)
- [Death to the Stock Photo](http://join.deathtothestockphoto.com/)
- [Little Visuals](http://littlevisuals.co/)

## With Gulp

- [Gulp Imagemin](https://github.com/sindresorhus/gulp-imagemin)  
- [Compass Sprite Generator](http://compass-style.org/help/tutorials/spriting/)
- [Favicon Generator](https://github.com/haydenbleasel/favicon-generator)
