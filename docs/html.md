# HTML

### Templates Engine

- [Jade](http://jade-lang.com/)
- [Halm](http://haml.info/)
- [Handlebars](http://handlebarsjs.com/)

### Email

- [Responsive email pattern](http://responsiveemailpatterns.com/)  
- [Ink by Zurb](http://zurb.com/ink/)

### With Gulp

- [Gulp HTMLMin](https://github.com/jonschlinkert/gulp-htmlmin)

### With Grunt
Feel fre to add.

### To read

- [Dive into HTML5](http://diveintohtml5.info/) [(PT-BR)](http://diveintohtml5.com.br/)  