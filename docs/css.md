# CSS

### Resets

- [Normalize CSS](http://necolas.github.io/normalize.css/)
- [YUI Reset](http://yuilibrary.com/yui/docs/cssreset/)

### Frameworks

- [Bootstrap](http://getbootstrap.com/)
- [Foundation](http://foundation.zurb.com/)
- [Pure](http://purecss.io)
- [Gumby](http://gumbyframework.com/)
- [UIKIT](http://www.getuikit.com/)
- [Semantic UI](http://semantic-ui.com/)

### Preprocessor

- [SASS](http://sass-lang.com/)
- [LESS](http://lesscss.org/)
- [Stylus](http://learnboost.github.io/stylus/)
- [Myth](http://www.myth.io/)
- [AbsurdJS CSS Preprocessor](http://absurdjs.com/pages/css-preprocessing/)

### With preprocessor

#### Sass

- [Compass](http://compass-style.org/)
- [Bourdon](http://bourbon.io/)
- [Neat](http://neat.bourbon.io/)
- [Bitters](http://bitters.bourbon.io/)
- [Refills](http://refills.bourbon.io/)

#### Less

- [Less element](http://lesselements.com/)

#### Stylus

- [Nib](http://visionmedia.github.io/nib/)

### Others

- [Layer Style](http://layerstyles.org/builder.html)
- [Ceaser](http://matthewlein.com/ceaser/)
- [CSS3 Test](http://css3test.com/)
- [Cssynth](http://bennettfeely.com/cssynth)
- [Snap SVG](http://snapsvg.io/)
- [Kalei Style Guide](http://kaleistyleguide.com)


- [Effekt.css](http://h5bp.github.io/Effeckt.css/)
- [Animated.css](http://daneden.github.io/animate.css/)
- [CSShacke](http://elrumordelaluz.github.io/csshake/)
- [Magic animation](http://www.minimamente.com/example/magic_animations/)


- [Auto Prefixer](https://github.com/ai/autoprefixer)
- [Express Prefix](http://expressprefixr.herokuapp.com/)

####[Westciv tools](http://westciv.com/)
- [Transforms](http://westciv.com/tools/transforms/index.html)
- [Linear Gradients](http://westciv.com/tools/gradients/index.html)
- [Radial Gradients](http://westciv.com/tools/radialgradients/index.html)

### Icons

- [Github Icon Font showcase](https://github.com/showcases/icon-fonts)
- [Font Awesome](http://fortawesome.github.io/Font-Awesome/)
- [Ionicons](http://ionicons.com/)
- [Icon Moon](http://icomoon.io/)
- [Fontello](http://fontello.com/)

### With Gulp

- [Minify CSS](https://github.com/jonathanepollack/gulp-minify-css)
- [Uncss](https://github.com/ben-eb/gulp-uncss)
- [CSS Beatify](https://www.npmjs.org/package/gulp-cssbeautify/)
- [CSS Lint](https://www.npmjs.org/package/gulp-csslint/)
- [Gulp Rework](https://www.npmjs.org/package/gulp-rework/)


- [Gulp SASS](https://github.com/dlmanning/gulp-sass)
- [Gulp Compass](https://github.com/appleboy/gulp-compass)
- [Gulp LESS](https://www.npmjs.org/package/gulp-less/)
- [Gulp Stylus](https://www.npmjs.org/package/gulp-stylus/)

### With Grunt
Feel fre to add.

### Tools
- [Pesticide](http://pesticide.io)

### To read

- [Smacss](http://smacss.com/)
- [BEM](http://bem.info/method/)
- [OOCSS](http://oocss.org/)
- [Idiomatic CSS](https://github.com/necolas/idiomatic-css)
- [Magic of CSS](http://adamschwartz.co/magic-of-css/)