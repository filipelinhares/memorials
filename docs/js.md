#Javascript

### Frameworks
- [YUI](http://yuilibrary.com)
- [jQuery](http://jquery.com)
- [Zepto]() [PT-BR](http://zeptojs.com.br/)
- [Rye](http://ryejs.com/)

### MV*

- [Todo MVC](http://todomvc.com/)
- [Backbone](http://backbonejs.org/)
- [Angular](http://angularjs.org/)
- [Spine](http://spinejs.com/)
- [Ember](http://emberjs.com/)
- [Knockout](http://knockoutjs.com/)
- [Vue](http://vuejs.org/)
- [React](http://facebook.github.io/react/)
- [Reactive](http://www.ractivejs.org/)

### Plugin jQuery

- [PermitJS](http://technotarek.com/permit/permit.html)
- [Flicker](http://getwebplate.com/plugins/flickerplate)
- [Sidr](http://www.berriart.com/sidr/#development)
- [Plax](http://cameronmcefee.com/plax/)
- [VideoGL](http://plugincc.fabiobiondi.com/?cerchez-project=videogl)
- [Baseline](https://daneden.me/baseline/)
- [TaggingJS](http://sniperwolf.github.io/taggingJS/)
- [jQuery Sticky Alert](https://github.com/tlongren/jquery-sticky-alert)
- [jQuery Transit](http://ricostacruz.com/jquery.transit/)
- [Velocity.js](http://julian.com/research/velocity/)

### Others

- [Underscore](http://underscorejs.org/)
- [Lo Dash](http://lodash.com/)
- [JSON Select](http://jsonselect.org/)
- [FuseJS](http://kiro.me/projects/fuse.html)
- [Oauth](https://oauth.io/)
- [PourOver](http://nytimes.github.io/pourover/)
- [Conditionizr](http://conditionizr.com/)
- [Modernizr](http://modernizr.com/)
- [Yep Nope](http://yepnopejs.com/)
- [Storage](https://github.com/ask11/storage)
- [CreateJS](http://www.createjs.com/)
- [Rivets](http://www.rivetsjs.com/)
- [Stapes](http://hay.github.io/stapes/)
- [Shine](http://bigspaceship.github.io/shine.js/)
- [RowGrid.js](http://brunjo.github.io/rowGrid.js/)
- [VisJS](http://visjs.org/)
- [Masonry](http://masonry.desandro.com/)
- [ProgressJS](http://usablica.github.io/progress.js/)
- [FileSaver](https://github.com/eligrey/FileSaver.js)
- [Geoip Server](https://github.com/aredo/simple-server-geoip)
- [Draggabilly](http://draggabilly.desandro.com/)
- [LazyLoad](https://github.com/rgrove/lazyload/)
- [Isotope](http://isotope.metafizzy.co/)
- [MotionJS](http://motionjs.com/index.html)
- [FramerJS](http://framerjs.com/learn.html#basics)
- [Responsive Tables](http://gergeo.se/RWD-Table-Patterns/)
- [RestiveJS](http://restivejs.com/)
- [Helium CSS](https://github.com/geuis/helium-css)

### RegExp

- [Regexr](http://regexr.com/)

### Compile to JS

- [CoffeeScript](http://coffeescript.org/)
- [TypeScript](http://www.typescriptlang.org/)

### with Gulp

- [Uglify](https://www.npmjs.org/package/gulp-uglify)
- [JSHint](https://www.npmjs.org/package/gulp-jshint)
- [JS Concat](https://www.npmjs.org/package/gulp-concat)

### with Grunt
Feel fre to add.

### To read

- [Javascript the right way](http://jstherightway.org/)
- [Idiomatic JS](https://github.com/rwaldron/idiomatic.js/)
