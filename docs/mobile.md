# Mobile

### Frameworks

- [ThumbJS](http://mwbrooks.github.io/thumbs.js/)
- [Mobile Framework Comparation](http://mobile-frameworks-comparison-chart.com/)
- [Ionic Framework](http://ionicframework.com/)
- [Ratchet](http://goratchet.com/)
- [Chocolatechip UI](http://chocolatechip-ui.com/)
- [Framework7](https://github.com/nolimits4web/framework7/)
- [jQuery Mobile](http://jquerymobile.com/)
- [Lungo](http://lungo.tapquo.com/)

### Resolution

- [Screensiz](http://screensiz.es/phone)