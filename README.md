# Memorials

####A Repository with tools to front end developers.

---

###In Progress

✔ [HTML](docs/html.md)  
✔ [CSS](docs/css.md)  
✔ [JavaScript](docs/js.md)  
✔ [Image](docs/image.md)  
✔ [Mobile](docs/mobile.md)  
✔ [Typograph](docs/typograph.md)

#Contribute
1. Fork it!
2. Create your branch: `git checkout -b my-new-branch`
3. Commit your changes: `git commit -m 'Add some feature'`
4. Push to the branch: `git push origin my-new-branch`
5. Submit a pull request :D

#Author

[Filipe Linhares](https://plus.google.com/+filipelinharesplus/)

#License

MIT © Filipe Linhares